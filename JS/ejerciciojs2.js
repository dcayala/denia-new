

function BuscarPorNombres(pais){
    var peticion = 'https://restcountries.eu/rest/v2/name/'+pais;
    fetch(peticion)
    .then(function(res){
        return res.json();
  })
  .then(function(res){
      var pais = {};
      var tabla = document.getElementById('tabla');
      LimpiarTabla(tabla);
      for(let index = 0; index < res.length; index++){
          pais.nombre = res[index].name;
          pais.dominio = res[index].topLevelDomain;
          pais.capital = res[index].capital;


          var tableData = '<td>'+pais.nombre+'</td><td>'
          +pais.dominio+'</td><td>'+pais.capital+'</td>';
           
          var tablerow = document.createElement('tr');
          tablerow.innerHTML = tableData;

          tabla.appendChild(tablerow);

      }
  
})
.catch(err=>console.error(err));
}


function LimpiarTabla(tabla){
    var filas = tabla.getElementsByTagName('tr');

    while(tabla.getElementsByTagName('tr').length >1){
        tabla.removeChild(filas[1]);
    }
}