//Entra en conflicto js con html si se declara dos variables con nombres iguales
//Variables del tipo Simple
var edad = 12;
var nombrep = "John";
var esAlto = true;

//Un objeto es una representacion de muchos tipos de en uno soplo

var persona = {
    edad: 12,
    nombre: 'Pepe',
    apellido: 'Perez',
    esAlto: true,

    Saluda: function () {
        return 'Hola'
    },

    NombreCompleto: function () {
        return 'El nombre completo de las persona es'
        this.nombre + ' ' + this.apellido
    },

    InformacionGeneral: function () {
        return this.NombreCompleto() + ' , y su edad es ' + this.edad
        this.nombre + ' ' + this.apellido
    }
}

//Impresion del obejto
// console.log(persona);

// //Impresion de los atributos del objeto
// console.log(persona.nombre);
// console.log(persona['edad']);
// console.log(persona.Saluda());
// console.log(persona.NombreCompleto());
// console.log(persona.InformacionGeneral());

function InformacionUsuario(nombre, apellido) {

    var usuario = {
        user: nombre,
        apell: apellido,

        completo: function () {
            return this.user + ' ' + this.apell
        }
    }

    var result = document.getElementById('resultado');
    result.innerText = usuario.completo();

}

function CargarPaises() {
    var peticion = new Request('https://restcountries.eu/rest/v2/all');

    fetch(peticion)
        .then(function (res) {
            return res.json();
        })
         .then(function (res) {

        //     var objetoR = res.name;
        //     document.getElementById('table').innerText = objetoR;
        //     console.log(res)

            var tabla = document.getElementById('tabla');

            for (let index = 0; index < res.length; index++) {
                var fila = '<tr><td>' + res
                [index].name + '</td><td>' + res
                [index].topLevelDomain + '</td><td>' + res[index].capital + '</td></tr>';

                var elemento = document.createElement('tr');
                elemento.innerHTML = fila;

                tabla.appendChild(elemento);
            }

        })
        .catch(err => console.log('Hubo un pequeño error: ' + err));


} 